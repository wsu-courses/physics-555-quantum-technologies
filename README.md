Phys 555 - Quantum Technologies
===============================
[![Documentation Status](https://readthedocs.org/projects/physics-555-quantum-technologies/badge/?version=latest)](https://physics-555-quantum-technologies.readthedocs.io/en/latest/?badge=latest)
[![gitlab pipeline status](https://gitlab.com/wsu-courses/physics-555-quantum-technologies/badges/main/pipeline.svg)](https://gitlab.com/wsu-courses/physics-555-quantum-technologies/-/commits/main)
[![gitlab coverage report](https://gitlab.com/wsu-courses/physics-555-quantum-technologies/badges/main/coverage.svg)](https://gitlab.com/wsu-courses/physics-555-quantum-technologies/-/commits/main)

This is the main project for the [WSU Physics][] course
**Phys 555: Quantum Technologies and Computing** first offered in [Fall 2022](https://schedules.wsu.edu/List/Pullman/20223/Phys/555/01).

# Quantum Notes

These will be moved to a better place... but for now they suffice:

## Entanglement: Correlation and Causation

Entanglement in quantum mechanics provides a means to ensure correlation between remote
observables without any implication of causality, in the sense that the entanglement
cannot be used to sent information.

Note however that [quantum correlation can imply
causation](https://www.sciencedaily.com/releases/2015/03/150323150645.htm), in the sense
that detecting quantum correlations can provide information about prior causation (the
preparation of the entanglement).

## Landau-Zener: A Core Example

I am toying with the idea of using the Landau-Zener transition as a core example to
demonstrate how to work with quantum mechanics.  There are analytic solutions, but I do
not fully understand them yet, however, the numerical solution is quick and intuitive.
Is there a better example?

# Funding

<a href="https://www.nsf.gov"><img width="10%"
src="https://nsf.widen.net/content/txvhzmsofh/png/" />
</a>
<br>

Some of the material presented here is based upon work supported by the National Science
Foundation under [Grant Number 2012190](https://www.nsf.gov/awardsearch/showAward?AWD_ID=2012190). Any opinions, findings, and conclusions or
recommendations expressed in this material are those of the author(s) and do not
necessarily reflect the views of the National Science Foundation.

<!-- Links -->
[Anaconda Project]: <https://github.com/Anaconda-Platform/anaconda-project> "Anaconda Project"
[CoCalc]: <https://cocalc.com> "CoCalc: Collaborative Calculation and Data Science"
[Conda]: <https://docs.conda.io/en/latest/> "Conda: Package, dependency and environment management for any language—Python, R, Ruby, Lua, Scala, Java, JavaScript, C/ C++, FORTRAN, and more."
[GitHub CI]: <https://docs.github.com/en/actions/guides/about-continuous-integration> "GitHub CI"
[GitHub]: <https://github.com> "GitHub"
[GitLab]: <https://gitlab.com> "GitLab"
[Git]: <https://git-scm.com> "Git"
[Heptapod]: <https://heptapod.net> "Heptapod: is a community driven effort to bring Mercurial SCM support to GitLab"
[Jupyter]: <https://jupyter.org> "Jupyter"
[Jupytext]: <https://jupytext.readthedocs.io> "Jupyter Notebooks as Markdown Documents, Julia, Python or R Scripts"
[Mercurial]: <https://www.mercurial-scm.org> "Mercurial"
[Miniconda]: <https://docs.conda.io/en/latest/miniconda.html> "Miniconda is a free minimal installer for conda."
[MyST]: <https://myst-parser.readthedocs.io/en/latest/> "MyST - Markedly Structured Text"
[Read the Docs]: <https://readthedocs.org> "Read the Docs homepage"
[WSU Physics]: <https://physics.wsu.edu> "WSU Department of Physics and Astronomy"
[WSU Mathematics]: <https://www.math.wsu.edu/> "WSU Department of Mathematics and Statistics"
[`anaconda-project`]: <https://anaconda-project.readthedocs.io> "Anaconda Project: Tool for encapsulating, running, and reproducing data science projects."
[`anybadge`]: <https://github.com/jongracecox/anybadge> "Python project for generating badges for your projects"
[`conda-forge`]: <https://conda-forge.org/> "A community-led collection of recipes, build infrastructure and distributions for the conda package manager."
[`genbadge`]: <https://smarie.github.io/python-genbadge/> "Generate badges for tools that do not provide one."
[`mmf-setup`]: <https://pypi.org/project/mmf-setup/> "PyPI mmf-setup page"
[`pytest`]: <https://docs.pytest.org> "pytest: helps you write better programs"
[hg-git]: <https://hg-git.github.io> "The Hg-Git mercurial plugin"
[GitLab test coverage visualization]: <https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html>

<!-- End Links -->
