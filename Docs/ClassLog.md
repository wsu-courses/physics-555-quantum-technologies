---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.0
kernelspec:
  display_name: Python 3 (phys-555)
  language: python
  name: phys-555
---

Class Log
=========

Mon 22 Aug 2022
---------------
* Introduced course material, Syllabus, etc.
* Discussed some limitations of classical computing like [Landauer's
  principle](https://en.wikipedia.org/wiki/Landauer%27s_principle) that erasing
  information generates heat.
* Briefly discussed Big-O notation.
* Introduced some faculty (Michael Forbes, Mark Kuzyk, Fred Gittes, Kevin Vixie)
* Breakout to discuss Monty Hall problem, expected winnings from rolling 2 dice where
  prime number sums win \$10 but composites loose \$5.  Ended with mention of sock
  problem.
  
Notes: Need to breakup groups.  Try asking people to distribute such that each group
has:

1. A member with a Ph.D. in physics.
2. An undergraduate.
3. Someone from outside of physics.

Wed 25 Aug 2022
---------------

**Plan**:
* Postulates.
* Spin 1/2 discussion.
* Universal computing/Turing machines/NAND is complete for classical computing.
* Classical circuits.
* Show CoCalc assignment and how that works.
* Introductions from online participants.
* Linear algebra: Bases, Hermitian, Unitary matrices, SVD, Schmidt decomposition. *(Friday)*

Discussions from Hypothesis:
* [Turing
  machines](https://hyp.is/GHtvWCNgEe2GkAdAsgSc1w/docdrop.org/static/drop-pdf/Quantum-Computation-and-Quantum-Information-10th-Anniversary-Edition-by-Michael-A.-Nielsen-Isaac-L.-Chuang-z-lib.org--5Y4pP.pdf)
  and the Church-Turing thesis.
* [P vs NP](https://hypothes.is/a/L26XNiMeEe2s7FdMrCdAoA).

* [Current state (NISQ) of quantum
  hardware](https://hyp.is/TjtccCPsEe28wyNvdSb4gw/docdrop.org/static/drop-pdf/Quantum-Computation-and-Quantum-Information-10th-Anniversary-Edition-by-Michael-A.-Nielsen-Isaac-L.-Chuang-z-lib.org--5Y4pP.pdf)
  and several questions about why there are limitations and what can be done.

* [Information](https://hyp.is/U1Tg8iN8Ee2Yy7f4BbQYfg/docdrop.org/static/drop-pdf/Quantum-Computation-and-Quantum-Information-10th-Anniversary-Edition-by-Michael-A.-Nielsen-Isaac-L.-Chuang-z-lib.org--5Y4pP.pdf).
* [Error correction](https://hypothes.is/a/890_WCMgEe2uOfeKSQWtVg).
* [Philosophical debate about whether or not quantum mechanics is a mathematical
  framework](https://hypothes.is/a/14VsQiKmEe2Mj6_aYrpL6w).
* [Quantum cryptography](https://hypothes.is/a/VxC_eiJcEe24doeYy-XcIA).
* [RSA vs Symmetric keys](https://hypothes.is/a/sTMQJiM3Ee2xEb8nOaarOw)
