(sec:readings)=
Resources, Readings, and References
===================================

## Textbooks

The primary textbook for the course is {cite:p}`Nielsen:2010`.  This is a thorough but
quite accessible textbook with many reference and has served as the standard reference
in the field.

More accessible is [Quantum computing for the very curious], {cite:p}`Matuschak:2019`
which includes a set of review questions delivered over an extended time to help you
remember the concepts (they call it a "new mnemonic medium").  The direct technical
goal of this course is for you to understand in detail everything discussed in this
and the related articles.

(sec:linear_algebra_resources)=
## Linear Algebra

* [Essence of linear algebra][]: A great set of highly visual videos by [3Blue1Brown][]
  getting you up to abstract vector spaces.
* [MIT 18.06 Linear Algebra][]: A set of [video
  lectures](https://ocw.mit.edu/courses/18-06-linear-algebra-spring-2010/video_galleries/video-lectures/)
  and accompanying material for the MIT Linear Algebra course.
* [Qiskit Linear Algebra](https://qiskit.org/textbook/ch-appendix/linear_algebra.html):
  A short introduction that is part of the [Qiskit] platform.
* Appendix A of {cite:p}`Mermin:2007` has a nice short review of Dirac notation.

## Additional Quantum Computing and Information Resources
* [(John Preskill) Quantum Computing and the Entanglement
  Frontier](https://www.nist.gov/video/nist-colloquium-series-john-preskill-quantum-computing-and-entanglement-frontier):
  Fairly recent lecture (Feb 2022) giving a good overview of the state of the field.  By
  the end of the course you should be able to understand the details of this talk.  (For
  example, where the 85.4% comes from as was asked at the end of the talk.)

* [Qiskit Textbook](https://qiskit.org/learn): This is the textbook put out by IBM for
  learning how to use their Qiskit software development kit (SDK).  It has a pretty good
  introduction, and if you want to move forward with quantum computing, then it is worth
  having a look at [Qiskit] as it is one of the main platforms available for performing
  quantum computations on real platforms.
  
* [Introduction to Classical and Quantum Computing (Tom
  Wong)](https://www.thomaswong.net): {cite:p}`Wong:2022` provides a very accessible
  introduction to the field without any assumed prerequisites beyond trigonometry.

:::{margin}
For example, Mermin is adamant about calling qubits "Qbits" in analogy to "Cbits".
:::
* [Quantum Computer Science: An Introduction](https://www.cambridge.org/core/books/quantum-computer-science/66462590D10C8010017CF1D7C45708D7):
  {cite:p}`Mermin:2007` has an opinionated account of quantum computer science which
  provides an alternative and useful prespective.  In general, I recommend this as an
  alternate presentation for you to gain another perspective and secure your knowledge,
  but there are a few useful sections to read thoroughly, especially appendices A, B,
  and C.

* [Geometry of Quantum States: An Introduction to Quantum Entanglement](https://doi.org/10.1017%2F9781139207010):
  {cite:p}`Bengtsson:2017` has a mathematically sophisticated description of the
  geometry of quantum mechanics and entanglement.  The mathematics in this book does
  obscure some of the physics, but I think that it has the potential to provide some
  good insight.
  
## General Articles

The following are good general (not technical) articles that help place quantum
computing and its potential capabilities in context:

* [Das Sarma:2022 Quantum computing has a hype problem.](https://www.technologyreview.com/2022/03/28/1048355/quantum-computing-has-a-hype-problem/)

## Technologies

* [Discussion about Majorana Modes between Sankar Das Sarma and Chetan
  Nayak](https://www.youtube.com/watch?v=Q8CHms4ixYc).  If you are interested in this
  technology, be sure to consider [Sergey
  Frolov](https://www.youtube.com/user/zdfgdxfdfgfxghdfg)'s work which found issues with
  some of the reported technical claims at having identified Majorana modes.  (Sergey
  gave a thought-provoking colloquium here at WSU Fall 2022.)
  
## Quantum Computing Simulators

These are platforms that allow you to program and simulate running your programs as if
the were running on a quantum computer.  Ultimately the work you do year should be able
to be run on real quantum hardware when it is available.  If you are interested in
employment in the field of quantum computing, then it is probably important to have some
experience with one of these platforms.  Certification might help in some cases (but is
expensive and time-consuming).

* [Qiskit]: IBM's platform.  If you don't have a reason to prefer one of the other
  platforms, this is probably the best place to start.
  
  One of the exercises in this course will be to work through the Qiskit field guide:

  * [Learn quantum computing: a field guide][Qiskit field guide]


## Interactive Demonstrations

Here are some interactive demonstrations (some quantum):

* <https://www.falstad.com/mathphysics.html>
  * <https://www.falstad.com/ripple/>
  * <https://www.falstad.com/qm1d/>
* <https://acephysics.net/>: Nice interactive approach to learning about spin
  measurements, but rather annoying interface for simple exploring.  I suggest you work
  through this once after you think you understand spins and measurement.
 

### Classical Mechanics

If your physics background is a little rusty and you want to play with some classical
mechanics simulations to build intuition, these are fun:

* <https://www.myphysicslab.com/>
* <https://www.falstad.com/mathphysics.html>: Has some great simulations.  The [Ripple
  Tank](https://www.falstad.com/ripple/) is very impressive.  (Look at the low-pass filter
  for example.)


## Research

Here are some random references that might be of interest to various research projects:

* [How many quantum gates do gauge theories require?](https://arxiv.org/abs/2208.11789):
  An accounting of how many gates would be required to solve lattice gauge theories like
  QCD.  Unfortunately, the number is quite high.

(sec:references)=
References
==========

```{bibliography}
:style: alpha
```

[MIT 18.06 Linear Algebra]: <https://ocw.mit.edu/courses/18-06-linear-algebra-spring-2010/>
[3Blue1Brown]: <https://www.youtube.com/c/3blue1brown>
[Essence of linear algebra]: <https://www.youtube.com/playlist?list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab>
[Quantum computing for the very curious]: <https://quantum.country/qcvc>
[Qiskit]: <https://qiskit.org>
[Qiskit field guide]: <https://quantum-computing.ibm.com/composer/docs/iqx/guide/>
