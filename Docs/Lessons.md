---
myst:
  substitutions:
    NC: |-
      {cite:p}`Nielsen:2010`
---

(sec:lessons)=
# Lesson Plan

* *Week 1 / Aug 22*
  * Introductions.  What is computing?  What is quantum mechanics?
  * Linear algebra review.
  * Probability and expectation values.
  * Cbits and Qbits (qubits).
    * Spin 1/2.
  * https://quantum.country/qcvc
*Week 2 / Aug 29*
  * Postulates.
  * Single qubit dynamics.
  * EPR.
*Week 3 / Sep 5 (Labor day)*
  * Measurement Bell
*Week 4 / Sep 12*
  * Classical Circuits.  Models of Computing.  Universality. Complexity.
  * Density matrices.
  * Quantum Circuits.
  * Quantum Gates.
  * Universality.
  * No Cloning.
  * Entanglement.
*Week 5 / Sep 19*
  * https://quantum.country/teleportation
*Week 6 / Sep 26*
  * Quantum Search. Grover.
  
*Week 7 / Oct 3*
  * https://quantum.country/search
*Week 8 / Oct 10*
*Week 9 / Oct 17*
*Week 10 / Oct 24*
*Week 11 / Oct 31*
*Week 12 / Nov 7*
*Week 13 / Nov 14*
*Thanksgiving / Nov 21*
*Week 14 / Nov 28*
*Week 15 / Dec 5*
*Week 16 / Dec 12 (Dead week)*
:::




### Lectures

In general, content should be delivered in chunks of <20 min, with breaks
between.  See
[Why use Active
Learning?](https://ablconnect.harvard.edu/lecture-research#:~:text=Modern%20best%20practices%20strongly%20suggest,information%20(Bligh%2C%202000)).


:::{margin}
*Week 1 / Aug 22*
:::
For the first few lectures, gradually move through the class allowing people to
introduce themselves. Organize by groups: CS, Math, Undergraduate, etc.  At most, 3-5 people
should introduce themselves at a time in a dialogue.  Use this to break up the delivery
of material.

The following should also be introduced at some point, and can be used to break up more
technical material.

* Angular momentum conservation.  Spinning on a chair... related to symmetry.
  Spin-up/spin-down.  Idea of a decay (e.g. positronium) of a state with no angular
  momentum into $(\ket{\uparrow\downarrow} - \ket{\downarrow\uparrow})/\sqrt{2}$.
* 

**Lecture 1:**
  * Introduction: 
    * What is quantum mechanics?
      * Particles:
        * Wave/Particle Duality
        * Randomness and Measurement
        * Simple formulation in terms of Linear Algebra.
      * Breakout session led by physics profs. and grad students.  Have different
        computers in classroom in different breakout rooms for Zoom.  What do people
        think quantum means?
      * Superposition, Entanglement, Random, Measurement, Spooky action at a distance.
      * EPR and Bell.
      
    * What classical computing?
      * Turing machines. Models of computing.
    * What is quantum computing?
      * Provable speedups.
      * Fig. 4.1 {{NC}}.
      * Gottesman-Knill (§10.5.4 {{NC}}).
  * Canvas, CoCalc, ReadTheDocs, Hypothes.is.
  * 

:::{margin}
*Week 2 / Aug 29*
:::

**Readings**: {{NC}} §1

:::{margin}
*Week 3 / Sep 5 (Labor day)*
:::
:::{margin}
*Week 4 / Sep 12*
:::
:::{margin}
*Week 5 / Sep 19*
:::
:::{margin}
*Week 6 / Sep 26*
:::
:::{margin}
*Week 7 / Oct 3*
:::
:::{margin}
*Week 8 / Oct 10*
:::
:::{margin}
*Week 9 / Oct 17*
:::
:::{margin}
*Week 10 / Oct 24*
:::
:::{margin}
*Week 11 / Oct 31*
:::
:::{margin}
*Week 12 / Nov 7*
:::
:::{margin}
*Week 13 / Nov 14*
:::
:::{margin}
*Thanksgiving / Nov 21*
:::
:::{margin}
*Week 14 / Nov 28*
:::
:::{margin}
*Week 15 / Dec 5*
:::
:::{margin}
*Week 16 / Dec 12 (Dead week)*
:::


## Textbook Tour

Here we provide a brief tour of the textbooks:

### Nielsen and Chuang {cite:p}`Nielsen:2010`

* §1: Introduction and overview: **Read!**

  *Excellent introduction and overview of quantum computing and information theory.*
  :::{toggle}
  * §1.1: Global Perspectives
  * §1.2: Quantum bits
  * §1.3: Quantum computation
    * §1.3.1: Single qubit gates
    * §1.3.2: Multiple qubit gates
    * §1.3.3:  Measurements in bases other than the computational basis
    * §1.3.4: Quantum circuits
    * §1.3.5: Qubit copying circuit?
    * §1.3.6: Example: Bell states
    * §1.3.7: Example: quantum teleportation
  * §1.4 Quantum algorithms
    * §1.4.1: Classical computations on a quantum computer
    * §1.4.2: Quantum parallelism
    * §1.4.3: Deutsch’s algorithm
    * §1.4.4: The Deutsch–Jozsa algorithm
    * §1.4.5: Quantum algorithms summarized
  * §1.5: Experimental quantum information processing
    * §1.5.1: The Stern–Gerlach experiment
    * §1.5.2: Prospects for practical quantum information processing
  * §1.6: Quantum information
    * §1.6.1: Quantum information theory: example problems
    * §1.6.2: Quantum information in a wider context
  :::
* §2: Introduction to quantum mechanics: **Read!**

  *Self-contained formulation of quantum mechanics.  This chapter contains the technical
  details you need to know.*
  :::{toggle}
  * §2.1: Linear algebra
  * §2.2: The postulates of quantum mechanics
  * §2.3: Application: superdense coding
  * §2.4: The density operator
    * §2.4.1: Ensembles of quantum states
    * §2.4.2: General properties of the density operator
    * §2.4.3: The reduced density operator
  * §2.5: The Schmidt decomposition and purifications
  * §2.6: EPR and the Bell inequality
  :::
* §3: Introduction to computer science: **Read!**

  *In order to understand what quantum computers can do better than classical computers,
  we must first understand what classical computers can do.*
  :::{toggle}
  * §3.1: Models for computation
    * §3.1.1: Turing machines
    * §3.1.2: Circuits
  * §3.2: The analysis of computational problems
    * §3.2.1: How to quantify computational resources
    * §3.2.2: Computational complexity
    * §3.2.3: Decision problems and the complexity classes P and NP
    * §3.2.4: A plethora of complexity classes
    * §3.2.5: Energy and computation 
  * §3.3: Perspectives on computer science
  :::
* §4: Quantum circuits: **Read!**

  *What is a quantum computer?*
  :::{toggle}
  * 4.1: Quantum algorithms
  * 4.2: Single qubit operations
  * 4.3: Controlled operations
  * 4.4: Measurement
  * 4.5: Universal quantum gates
    * 4.5.1: Two-level unitary gates are universal
    * 4.5.2: Single qubit and CNOT gates are universal
    * 4.5.3: A discrete set of universal operations
    * 4.5.4: Approximating arbitrary unitary gates is generically hard
    * 4.5.5: Quantum computational complexity
  * 4.6: Summary of the quantum circuit model of computation
  * 4.7: Simulation of quantum systems
    * 4.7.1: Simulation in action
    * 4.7.2: The quantum simulation algorithm
    * 4.7.3: An illustrative example
    * 4.7.4: Perspectives on quantum simulation
* 5: The quantum Fourier transform and its applications

  :::{dropdown} *Very interesting but...*
  
  The quantum Fourier transform is the key to Shor's factoring algorithm, arguably one
  of the most important discoveries driving the current quantum interest.  Shor's
  quantum algorithm for factoring composite integers has the potential to allow
  quantum computes to break classical cryptography, driving a large interest and
  investment based on NSA concerns (see the introduction of Chapter 6 of
  {cite:p}`Williams:2011` for a fun account).
  
  Technically, however, understanding this algorithm will require background and time
  that we simply do not have in a single semester course.  This would make a good topic
  for a project.
  
  Two points in defense of skipping this topic:
  1. A realistic implementation requires enormous resources (number of high-quality
     qubit) and is likely very far away.
  2. This algorithm is not provably faster than classical algorithms: we simply have not
     found fast classical algorithms yet.
  :::
  
* 6: Quantum search algorithms
* 7: Quantum computers: physical realization
  *This chapter is a good starting point for graduate student projects.*
* 8: Quantum noise and quantum operations
* 9: Distance measures for quantum information
* 10: Quantum error-correction
* 11: Entropy and information
* 12: Quantum information theory
* Appendix 1: Notes on basic probability theory
* Appendix 2: Group theory
  *Needed for understanding Shor's algorithm.*
* Appendix 3: The Solovay--Kitaev theorem
* Appendix 4: Number theory
* Appendix 5: Public key cryptography and the RSA cryptosystem
* Appendix 6: Proof of Lieb’s theorem

### Mermin {cite:p}`Mermin:2007`

:::{margin}
For example, Mermin is adamant about calling qubits "Qbits" in analogy to "Cbits".
:::
This is a highly opinionated account of quantum computer science, but provides an
alternative and useful prespective.  In general, I recommend this as an alternate
presentation for you to gain another perspective and secure your knowledge, but there
are a few useful sections to read:

* §6: Protocols that use just a few Qbits
  
  *Provides an interesting and compact presentation of Bell states, superdense coding,
  and teleportation.*

* §Appendix A: Vector spaces: basic properties and Dirac notation

  *Another review of the requisite linear algebra.*
* §Appendix B: Structure of the general 1-Qbit unitary transformation

  *A detailed presentation of Eqs. (4.7), (4.8), and (4.9) from {{NC}} and discussion
  about why they are useful.  See {ref}`sec:BlochSphere`.*
  
* §Appendix C: Structure of the general 1-Qbit state

  *Related discussion about states. See {ref}`sec:BlochSphere`.*
* §Appendix D: Spooky action at a distance

  *Somewhat technical chapter dealing with the EPR paradox expressed in a very concrete
  way.  We will likely work through this in class.  It should be a good use of density matrices.*
* §Appendix E: Consistency of the generalized Born rule.
