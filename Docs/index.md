<!-- Phys 555 - Quantum Technologies
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
-->

# Physics 555: Quantum Technologies and Computation

Welcome to the course page for Physics 555: Quantum Technologies and Computation.  This
course, originally offered in the Fall term of 2022, is designed to give you the solid
background required to understand the essence of quantum theory underlying quantum
technologies like advanced sensors and simulators, and quantum computation.

It is not intended to replace a traditional physics education in quantum mechanics such
as offered in other courses like Physics 450, 550, 551, and 552, but will focus on key
concepts needed to understand what makes quantum physics different from classical
physics, and how these differences might be harvested to enable technological advances.

## About These Notes

:::{margin}
Keep in mind that open-source documentation is much more difficult than open-source
code.  I expect this is in no small part due to the difficulty in "testing".  The
[Software Carpentry][] project has experience in this direction.
:::
These notes are not (yet) intended to represent a complete course, textbook, etc.  They
are very much a work in progress, but I like to make my thoughts and ideas available for
discussion.  If you have comments, ideas, and/or would like to contribute, please reach
out to me at <m.forbes@wsu.edu>.  The notes themselves live at [GitLab][] and you can
contribute in an open-source manner.

## Course Overview

This website, hosted on [Read The Docs](https://physics-555-quantum-technologies.readthedocs.io/en/latest), will be used to
collect and display additional information about the course, including:
* {ref}`sec:sylabus`
* {ref}`sec:assignments`
* {ref}`sec:readings`

and various class notes.  These should also be available through the navigation menu
(which might hidden if your display is not sufficiently wide).

These documents are built using [JupyterBook]() (see {ref}`sec:demonstration`) and
include all of the source code needed to generate the figure, plots etc.  For example,
to see how a figure was made, look in the preceding code cell.
The complete source code for this documentation is available at
<https://gitlab.com/wsu-courses/physics-555-quantum-technologies>. 

## Funding Statement
<a href="https://www.nsf.gov"><img width="10%"
src="https://nsf.widen.net/content/txvhzmsofh/png/" />
</a>
<br>

Some of the material presented here is based upon work supported by the National Science
Foundation under [Grant Number 2012190](https://www.nsf.gov/awardsearch/showAward?AWD_ID=2012190). Any opinions, findings, and conclusions or
recommendations expressed in this material are those of the author(s) and do not
necessarily reflect the views of the National Science Foundation.
 



Instructors: the information presented here at the start of the course documentation is
contained in the `Docs/index.md` file, which you should edit to provide an overview of
the course.

The first part of the course will establish the required formal background in linear
algebra, computer science, and give the essence of quantum computing.  The various
textbooks and resources listed in {ref}`sec:readings` should be your primary reference,
with active discussions taking place using [Hypothes.is] through a private group.  *(See
[the first reading
assignment](https://wsu.instructure.com/courses/1574702/assignments/7542709) for details
about how to setup [Hypothes.is] and how to register to this group.)*
These annotations and associated discussions will drive the lectures where we will
address conceptual difficulties, and discuss interesting features and implications of
quantum technologies.

The second part of the course will focus on various quantum technologies, with
presentations from students and guest lectures tailored to the interests of the class.

These notes will expand on the readings, providing additional perspective, numerical
examples, and documenting discussions that arise during the course.

## Prerequisites

:::{margin}
Being able to **follow** an argument is not indicative of understanding.  True
understanding will allow you to **predict** the outcome in unfamiliar circumstances.
:::
Understanding quantum mechanics requires some mathematical sophistication -- at a
minimum, complex numbers and linear algebra.  While the internet is rife with popular
accounts of mysterious quantum behaviour whose arguments can be **followed** without the
requisite mathematical background, acquiring a robust **predictive** understanding of
these phenomena quantum requires a strong basis in linear algebra.  Without a proper
foundation, one can easily mistake cause and effect in quantum mechanics, leading to
incorrect and paradoxical conclusions.  (See {ref}`sec:misconceptions` for some
examples.)

:::{important}
Please review {ref}`sec:prerequisites` as soon as possible, and if anything is
unfamiliar, ask questions or review.   This includes some {ref}`sec:administration` if
you are taking the course officially.
:::



```{toctree}
---
maxdepth: 2
caption: "Contents:"
titlesonly:
hidden:
---
Syllabus
Assignments

References
```
```{toctree}
---
maxdepth: 2
caption: "Prerequisites:"
titlesonly:
hidden:
glob:
---
Prerequisites/*
```


```{toctree}
---
maxdepth: 2
caption: "Prerequisites:"
hidden:
glob:
---
Prerequisites
Prerequisites/*
```

```{toctree}
---
maxdepth: 2
caption: "Notes:"
hidden:
glob:
---
Notes/*
```

```{toctree}
---
maxdepth: 2
caption: "Projects:"
hidden:
glob:
---
Projects/*
```

<!-- ```{toctree} -->
<!-- --- -->
<!-- maxdepth: 2 -->
<!-- caption: "QFT: (Physics 581)" -->
<!-- hidden: -->
<!-- glob: -->
<!-- --- -->
<!-- QFT/* -->
<!-- ``` -->

<!-- ```{toctree} -->
<!-- --- -->
<!-- maxdepth: 2 -->
<!-- caption: "Classical Mechanics: (Physics 521)" -->
<!-- hidden: -->
<!-- glob: -->
<!-- --- -->
<!-- ClassicalMechanics/* -->
<!-- ``` -->

```{toctree}
---
maxdepth: 2
caption: "Miscellaneous:"
hidden:
---
Demonstration
CoCalc
ClassLog
Misconceptions
../InstructorNotes


README.md <../README>
```

```{toctree}
---
maxdepth: 2
caption: "Instructor Only:"
hidden:
glob:
---
Overview
InstructorOnly/*
```

<!-- If you opt to literally include files like ../README.md and would like to be able
     to take advantage of `sphinx-autobuild` (`make doc-server`), then you must make
     sure that you pass the name of any of these files to `sphinx-autobuild` in the
     `Makefile` so that those files will be regenerated.  We do this already for
     `index.md` but leave this note in case you want to do this elsewhere.
     
     Alternatively, you can include them separately and view these directly when editing.
     We do not include this extra toc when we build on RTD or on CoCalc.  We do this
     using the `sphinx.ext.ifconfig extension`:
     
     https://www.sphinx-doc.org/en/master/usage/extensions/ifconfig.html

```{eval-rst}
.. ifconfig:: not on_rtd and not on_cocalc

   .. toctree::
      :maxdepth: 0
      :caption: Top-level Files:
      :titlesonly:
      :hidden:

      README.md <../README>
      InstructorNotes.md <../InstructorNotes>
```
-->


[Read the Docs]: <https://readthedocs.org/projects/physics-555-quantum-technologies/>
[Canvas page]: <https://wsu.instructure.com/courses/1574702>
[CoCalc shared project]: <https://cocalc.com/projects/ebaafbe3-f8cf-4598-a2c9-e7b6c64023c4/files/.repositories/physics-555-quantum-technologies.hg/README.md>
[Announcements]: <https://wsu.instructure.com/courses/1574702/announcements>
[Assignments]: <https://wsu.instructure.com/courses/1574702/assignments>
[GitLab]: <https://gitlab.com/wsu-courses/physics-555-quantum-technologies>
[Hypothes.is]: <https://hypothes.is/>
[Software Carpentry]: <https://software-carpentry.org/>
