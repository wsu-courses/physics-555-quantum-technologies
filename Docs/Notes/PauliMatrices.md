---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.0
kernelspec:
  display_name: Python 3 (phys-555)
  language: python
  name: phys-555
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:PauliMatrices)=
# Pauli Matrices

Here we summarize some properties of the [Pauli matrices]:

\begin{gather*}
  \mat{\sigma}_1 = \mat{\sigma}_x = \mat{X} = \begin{pmatrix}
    0 & 1\\
    1 & 0
  \end{pmatrix},\\
  \mat{\sigma}_2 = \mat{\sigma}_y = \mat{Y} = \begin{pmatrix}
    0 & -\I\\
    \I & 0
  \end{pmatrix},\\
  \mat{\sigma}_3 = \mat{\sigma}_z = \mat{Z} = \begin{pmatrix}
    1 & 0\\
    0 & -1
  \end{pmatrix}.
\end{gather*}

We will often work with these as a "vector" with three components $\mat{\sigma}_{i}$
which will always have a Roman index $i \in \{x, y, z\}$:

\begin{gather*}
  \vec{\mat{\sigma}} = \mat{\sigma}_{i} = (\mat{\sigma}_x, \mat{\sigma}_y, \mat{\sigma}_z)
\end{gather*}

so we can write things like:

\begin{gather*}
  \vec{a}\cdot\vec{\mat{\sigma}} = a_x\mat{\sigma}_x + a_y\mat{\sigma}_y + a_z\mat{\sigma}_z.
\end{gather*}

These "vectors" with an arrow have nothing to do with "kets" in the Hilbert space.  They
typically represent the corresponding directions in 3D which considering the
{ref}`sec:BlochSphere`.

To these, it will be convenient to add the identity:

\begin{gather*}
  \mat{\sigma}_0 = \mat{1} = \begin{pmatrix}
    1 & 0\\
    0 & 1
  \end{pmatrix}.
\end{gather*}

We can then refer to the [Pauli 4-vector], which will always have a Greek index $\mu \in
\{0,x,y,z\}.

\begin{gather*}
  \mat{\sigma}_{\mu} = (\mat{\sigma}_0, \mat{\sigma}_x, \mat{\sigma}_y, \mat{\sigma}_z),
\end{gather*}.

## Key Properties

In the following, Greek indices refer to all four matrices (including the identity),
while Roman indices refer only to the strict Pauli matrices.  These are expressed in
terms of the Kronecker deltas (identity matrices):

\begin{gather*}
  \delta_{ij} = \begin{cases}
    1 & i = j \in \{x, y, z\}\\
    0 & \text{otherwise}
  \end{cases}, \qquad
  \delta_{\mu\nu} = \begin{cases}
    1 & \mu = \nu \in \{0, x, y, z\}\\
    0 & \text{otherwise}
  \end{cases},
\end{gather*}

and the [Levi-Civita symbol] which is $\pm 1$ if the indices are an even or odd
permutation of (1, 2, 3) respectively and $0$ if any index is repeated.

\begin{gather*}
  \epsilon_{ijk} = \begin{cases}
    +1 & (i, j, k) \in \{(1,2,3), (2,3,1), (3,1,2)\},\\
    -1 & (i, j, k) \in \{(3,2,1), (2,1,3), (1,3,2)\},\\
    0 & \text{otherwise}.
  \end{cases}
\end{gather*}

This appears in the cross product:

:::{margin}
Here and in what follows we use the [Einstein summation convention] where repeated
indices are summed.  Here we do not consider relativity, so there is no distinction
between raised and lowered indices.
:::
\begin{gather*}
  [\vec{a}\times\vec{b}]_{l} = \sum_{jk}\epsilon_{jkl}a_jb_k
  \equiv \epsilon_{jkl}a_jb_k.
\end{gather*}

\begin{gather*}
  \mat{\sigma}_{\mu} = \mat{\sigma}_{\mu}^\dagger, \qquad
  \mat{\sigma}_{\mu}^2 = \mat{1}, \\
  \Tr \mat{\sigma}_{\mu} = 2\delta_{\mu 0}, \qquad
  \Tr \mat{\sigma}_{i} = 0,\\
\end{gather*}

One of the most useful relationships is:

\begin{gather*}
  \mat{\sigma}_j \mat{\sigma}_k = \delta_{jk} \mat{1} + \I \epsilon_{jkl} \mat{\sigma}_{l}.
\end{gather*}

Thus:

\begin{gather*}
  (\vec{a}\cdot\vec{\mat{\sigma}})(\vec{b}\cdot\vec{\mat{\sigma}})
  = (a_j\mat{\sigma}_j)(b_k\mat{\sigma}_k)
  = a_jb_k(\mat{\sigma}_j\mat{\sigma}_k)
  = a_jb_k(\delta_{jk} \mat{1} + \I \epsilon_{jkl} \mat{\sigma}_{l})\\
  = \vec{a}\cdot\vec{b}\mat{1} + \I (\vec{a}\times\vec{b})\cdot\vec{\mat{\sigma}}.
\end{gather*}

We also have 













[Pauli matrices]: <https://en.wikipedia.org/wiki/Pauli_matrices>
[Pauli 4-vector]: <https://en.wikipedia.org/wiki/Pauli_matrices#Pauli_4-vector>
[Levi-Civita symbol]: <https://en.wikipedia.org/wiki/Levi-Civita_symbol>
[Einstein summation convention]: <HTTP://en.wikipedia.org/wiki/Einstein_notation>
