---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (phys-555)
  language: python
  name: phys-555
---

```{code-cell} ipython3
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(entanglement)=
# Entanglement

One of the key features of quantum mechanics that distinguishes it from classical
mechanics is the notion of **entanglement**.  Intuitively, entanglement is a form of
correlation between separated measurements of a quantum system.

**Entanglement is correlation without causation.**

Consider two spin-½ particles in a singlet state:
\begin{gather*}
  \ket{\psi_0} = \frac{\ket{↑↓} - \ket{↓↑}}{\sqrt{2}}
               = \frac{\ket{01} - \ket{10}}{\sqrt{2}}.
\end{gather*}
The idea is that Alice has access to the first qubit, and Bob has access to the second
qubit.  Formally, Alice and evolve/measure operators of the form $\op{A} =
\op{M}\otimes\mat{1}$ and Bob can evolve/measure operators of the form $\op{B} =
\op{1}\otimes\op{M}$: i.e. single-qubit operations.  To produce the state $\ket{\psi_0}$
from, say, $\ket{00}$, one needs genuine two-qubit operators such as ᴄɴᴏᴛ.

````{admonition} Do It! Find a circuit to produce a singlet state from $\ket{00}$.
:class: dropdown

These states are said to be **maximally entangled**, from the perspective of Alice and Bob.
````
```{code-cell}
:tags: [hide-cell]

from qiskit.quantum_info import Statevector, Operator
from qiskit import QuantumCircuit
qc = QuantumCircuit(2)
qc.reset([0, 1])
qc.h(0)
qc.cnot(0,1)
qc.y(0)
display(qc.draw('mpl'))
psi = Statevector.from_label('00')
display(psi.evolve(qc).draw('latex'))
#display(array_to_latex(Operator(qc).data))
```


## Bell Inequalities


## Single Particle Entanglement

*This section discusses wavefunctions, which are not a part of the course, but very
familiar to physicists.  Please ignore for now unless this interests you.  Consider it a
work it progress that may be migrated to finite-dimensional Hilbert spaces later.*

Here we develop the following idea: suppose we have a single particle on the real line,
described by a wavefunction $\psi(x)$.  Define the projectors onto the left and right
subspaces:

\begin{gather*}
  \op{P}_{L} = \int_{-\infty}^{0} \d{x} \ket{x}\bra{x}, \qquad
  \op{P}_{R} = \int_{0}^{\infty} \d{x} \ket{x}\bra{x}.
\end{gather*}

The key property of these projectors is that they are [idempotent], and in our case,
they are complete (they partition the space into two orthogonal subspaces):

\begin{gather*}
  \op{P}_{R,L}^2 = \op{P}_{R,L}, \qquad \op{P}_{L} + \op{P}_{R} = \op{1}.
\end{gather*}

This means that the eigenvalues are either 0 or 1.  The span of the eigenvectors with
eigenvalue 1 form a subspace onto which the projector projects vectors.

:::{margin}
Prove that any complete set of projectors satisfy the properties of a {ref}`POVM`.
:::
These projectors define a {ref}`POVM` with two outcomes: L that the particle is on the
left ($x<0$) and R that the particle is on the right.

Now we can ask: can we define a notion of entanglement between the left (L) and right
(R) subspaces?  If we follow the arguments in {cite:p}`Klich:2006`, we can proceed as
follow:

:::{margin}
Assume that both overlaps are non-zero that the denominator never vanishes.  Otherwise,
pick any normalized state with support on the appropriate side.
:::
1. Suppose the particle is in state $\ket{\psi_0}$ with wavefunction $\psi_0(x) =
   \braket{x|\psi_0}$.  Define the following orthonormal states:
   
   \begin{gather*}
     \ket{L_0} =
     \frac{\op{P}_{L}\ket{\psi_0}}{\sqrt{p_L}}, 
     \qquad
     \ket{R_0} =
     \frac{\op{P}_{R}\ket{\psi_0}}{\sqrt{p_R}},\\
     p_L = \braket{\psi_0|\op{P}_{L}|\psi_0}, \qquad
     p_R = \braket{\psi_0|\op{P}_{R}|\psi_0}.
   \end{gather*}
   
   These are the states that a measurement of $\ket{\psi_0}$ would yield, with
   probabilities $p_L$ and $p_R$ respectively.
2. 
   

## Artificial Single-Particle Entanglement

Consider a single particle with wavefunction $\psi(x)$ factored as:

\begin{gather*}
  \psi(x) = \begin{cases}
    \psi_L(-x) & x \leq 0\\
    \psi_R(x) & x > 0.
  \end{cases}.
\end{gather*}

We can think of this as a tensor product of a qubit with a particle moving on the
positive half-line $x\geq 0$.  Thus, we can write, for $x\geq0$:

:::{margin}
Get good notation for spaces...
:::
\begin{gather*}
  \psi_{\sigma}(x) = \begin{pmatrix}
    \psi_{L}(x)\\
    \psi_{R}(x)
  \end{pmatrix} 
  \in \overbrace{\mathbb{SP}_2}^{A} \otimes \overbrace{\mathbb{H}^{+}_{\infty/2}}^{B}.
\end{gather*}

Note that this wavefunction has two indices: an index $\sigma \in \{L, R\}$, and a
"spatial index" $x\in[0, \infty)$.  This demonstrates the tensor-product structure of
the space.  The full density matrix $\mat{\rho}$ thus has four indices grouped into pairs:

\begin{gather*}
  [\mat{\rho}]_{\sigma,x;\sigma',x'}=\psi_{\sigma}(x)\psi^*_{\sigma'}(x').
\end{gather*}

Thus, we can apply standard techniques and take the
partial trace to obtain a $2\times 2$ reduced density matrix

\begin{gather*}
  [\mat{\rho_{A}}]_{\sigma \sigma'} 
  = \Tr_{B}(\mat{\rho})
  = \int \d{x}\psi_{\sigma}(x)\psi_{\sigma'}(x)\\
  = \int_0^{\infty} \d{x}
  \begin{pmatrix}
    \psi_L(x)\psi_L^*(x) & \psi_L(x)\psi_R^*(x)\\
    \psi_R(x)\psi_L^*(x) & \psi_R(x)\psi_R^*(x)
  \end{pmatrix}.
\end{gather*}

Let's consider a few examples.  What about a wavefunction that has support only on the
right $\psi_L(x) = 0$ or $\psi(x) = \Theta(x)\psi(x)$ or only on the left $\psi_R(x) =
0$ or $\psi(x) = \Theta(-x)\psi(x)$?

\begin{gather*}
  \left.\mat{\rho_{A}}\right|_{\psi_L(x) = 0} =
  \begin{pmatrix}
    1 & 0\\
    0 & 0
  \end{pmatrix}, \qquad
  \left.\mat{\rho_{A}}\right|_{\psi_R(x) = 0} =
  \begin{pmatrix}
    0 & 0\\
    0 & 1
  \end{pmatrix}.
\end{gather*}

In these cases, it is obvious that the qubit exhibits no entanglement.  These are pure
density matrices with eigenvalues $\lambda \in \{0, 1\}$, hence the von Neumann entropy
is zero.

If we the wavefunction is even $\psi(x) = \psi(-x)$, then

\begin{gather*}
  \mat{\rho_{A}} = \frac{1}{2}
  \begin{pmatrix}
    1 & 1\\
    1 & 1
  \end{pmatrix}
\end{gather*}

This looks more promising, but the eigenvalues are still $0$ and $1$, so this not
entangled either.



:::{admonition} Instructor Notes

This discussion needs much more attention -- student's don't obviously make the
connection between eigenvectors, eigenvalues, von Neumann entropy, SVD, etc.

Mathematics students don't like viewing a vector as a matrix.
:::

\begin{gather*}
  \psi_{ij} = U_{ik}D_{kk}V^*_{jk}   = \sum_{k} \sigma_k \overbrace{U_{ik}}^{\ket{a_k}}\overbrace{V^*_{jk}}^{\ket{b_k}}\\
  \psi = \begin{pmatrix}
    \psi_{00}\\
    \psi_{01}\\
    \psi_{10}\\
    \psi_{11}
  \end{pmatrix}
  \equiv
  \begin{pmatrix}
    \psi_{00} & \psi_{01}\\
    \psi_{10} & \psi_{11}
  \end{pmatrix}\\
  \psi_{ij} = \ket{a}\otimes\ket{b}\\
\end{gather*}

### Bell

\begin{gather*}
  \ket{\psi} = \frac{\ket{00} + \ket{11}}{\sqrt{2}} \neq \ket{a}\otimes\ket{b}\\
  \mat{\rho} = \ket{\psi}\bra{\psi} = 
  \frac{1}{2}\bigl(\ket{00} + \ket{11}\bigr)\bigl(\bra{00} + \bra{11}\bigr) =\\
  =
  \frac{1}{2}
  \begin{pmatrix}
    1 & 0 & 0 & 1\\
    0 & 0 & 0 & 0\\
    0 & 0 & 0 & 0\\
    1 & 0 & 0 & 1
  \end{pmatrix},\\
  \mat{\rho_A} = 
  \frac{1}{2}
  \begin{pmatrix}
    1 & 0\\
    0 & 1
  \end{pmatrix}.
\end{gather*}













(POVM)=
## Positive Operator Valued Measurement (POVM)






[idempotent]: <https://en.wikipedia.org/wiki/Idempotence>
